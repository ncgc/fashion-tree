# Fashion Tree
![Image](https://img.shields.io/badge/Machine%20Learning-tree-blue)
![Image](https://img.shields.io/static/v1?label=type&message=Image%20Processing&color=yellow)

Comparison of rating performance using decision trees and random trees.

## Getting Started

Use the package manager pip to install libraries.

### Prerequisites to download dataset

`pip install Keras`

### Prerequisites to run 

`pip install pandas`
`pip install numpy`
`pip install -U scikit-learn`

## Results
More project comments can be seen on the **Wiki**.


## Warnings
Do not execute all command lines at once. Some cells takes a long time to fully 
execute. 


## Versioning
[v.0.1 ] In development

## Author
Natália Cardoso Gonçalves

## License
[MIT](https://choosealicense.com/licenses/mit/)